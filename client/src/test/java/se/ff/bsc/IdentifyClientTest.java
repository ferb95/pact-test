package se.ff.bsc;


import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.DslPart;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class IdentifyClientTest {
    @Rule
    public PactProviderRuleMk2 provider = new PactProviderRuleMk2("public-user-service", "localhost", 8112, this);

    @Pact(consumer = "UI")
    public RequestResponsePact createPact(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap();
        headers.put("Content-Type", "application/json");


     DslPart results = new PactDslJsonBody()
                .stringValue("name",null)
                .integerType("type", 3)          
                .asBody(); 

        return builder
                .given("There isn�t a client with document number: 39099309 and document type:96")
                .uponReceiving("A request to identify a person")
                .path("/public-users/identify")
                .query("documentType=96&documentNumber=39099309")
                .method("POST")            
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(results).toPact();

    }

    @Test
    @PactVerification()
    public void doTest() {
        System.setProperty("pact.rootDir","../pacts");  // Change output dir for generated pact-files
        Integer type = new IdentifyClient(provider.getPort()).checkClient("96", "39099309");
        System.out.println("Client type="+type);
        assertTrue(type == 3);
    }

}

