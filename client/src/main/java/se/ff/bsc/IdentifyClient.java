package se.ff.bsc;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

public class IdentifyClient
{
    int port=8080;

    IdentifyClient() {
        // Will use default port.
        System.out.println("Default port "+port);
    }

    IdentifyClient(int port) {
        this.port=port;
        System.out.println("Custom port "+port);
    }

    public static void main( String[] args ) {
        Integer type = new IdentifyClient().checkClient("96","39099309");
        System.out.println("type="+type);
    }

    public Integer checkClient(String documentType, String documentNumber) {
        try {
            String url=String.format("Http://localhost:%d/public-users/identify?documentType=%s&documentNumber=%s", port, documentType, documentNumber);
            System.out.println("using url: "+url);
            HttpResponse r = Request.Post(url).execute().returnResponse();
            String json = EntityUtils.toString(r.getEntity());
            System.out.println("json="+json);
            JSONObject jsonObject = new JSONObject(json);
            String type = jsonObject.get("type").toString();
            return new Integer(type);

        }
        catch (Exception e) {
            System.out.println("Unable to identify client, e="+e);
            return null;
        }
    }
}
